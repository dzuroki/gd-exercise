import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Listring } from '../home.page';

@Component({
  selector: 'app-data-access',
  templateUrl: './data-access.component.html',
  styleUrls: ['./data-access.component.scss'],
})
export class DataAccessComponent implements OnInit {

  @Input() data!: Listring;
  @Output() callBackData: EventEmitter<Listring> = new EventEmitter<Listring>();
  constructor() { }

  ngOnInit() {}

  doCallBack() {
    this.callBackData.emit(this.data);
  }

}
