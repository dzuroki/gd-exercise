import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private readonly baseApi = 'http://ec2-18-136-103-87.ap-southeast-1.compute.amazonaws.com:3000';

  constructor(private httpClient: HttpClient) { }

  getData() {
    return this.httpClient.get<any>(`${this.baseApi}/tasks/listing`);
  }

  createData(payload: any) {
    return this.httpClient.post<any>(`${this.baseApi}/tasks/create`, payload);
  }

  deleteData(id: string) {
    return this.httpClient.delete<any>(`${this.baseApi}/tasks/delete`, { body: { id }});
  }
}
