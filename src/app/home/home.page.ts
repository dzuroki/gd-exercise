import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';

export interface Listring {
  id: string;
  status: boolean;
  taskDesc: string;
  taskName: string;
}
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  dataListing: Listring[];
  textName: string;
  textDesc: string;

  constructor(private homeService: HomeService) {
    console.log('constructor');
  }

  ngOnInit(): void {
    console.log('ngOnInit');
    this.getData();
  }

  async getData() {
    try {
      const result = await this.homeService.getData().toPromise();
      this.dataListing = result.data;
    } catch (error) {
      alert(error);
    }
  }

  async doCreate() {
    try {
      const payload = {
        taskName: this.textName,
        taskDesc: this.textDesc
      };
      const result = await this.homeService.createData(payload).toPromise();
      console.log(result);
      this.textName = '';
      this.textDesc = '';
      this.getData();
    } catch (error) {
      alert(error);
    }
  }

  async onCallBack(ev) {
    try {
      const result = await this.homeService.deleteData(ev.id).toPromise();
      console.log(result);
      this.getData();
    } catch (error) {
      alert(error);
    }
  }

}
